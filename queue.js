let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(e) {
  collection[collection.length] = e;
  return collection;
}

function dequeue() {
  let newCollection = [];
  for (let i = 1; i < collection.length; i++) {
    // for (let x = 0; x < collection.length; x++) {
    //   newCollection[x] = collection[i];
    //   //replaces the first array value but doesn't remove the last value
    // }
    newCollection[newCollection.length] = collection[i];
    collection = newCollection; //can reassign newCollection to collection 🤡
    return collection;
  }
}

function front() {
  if (collection.length !== 0) {
    return collection[0];
  } else {
    return null;
  }
}

function size() {
  return collection.length;
}

function isEmpty() {
  if (collection.length !== 0) {
    return false;
  } else {
    return true;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
